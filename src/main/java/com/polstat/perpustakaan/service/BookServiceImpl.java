package com.polstat.perpustakaan.service;

import com.polstat.soap.gen.BookDto;
import com.polstat.perpustakaan.entity.Book;
import com.polstat.perpustakaan.mapper.BookMapper;
import static com.polstat.perpustakaan.mapper.BookMapper.mapToBookDto;
import com.polstat.perpustakaan.repository.BookRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void createBook(BookDto bookDto) {
        bookRepository.save(BookMapper.mapToBook(bookDto));
    }

    @Override
    public List<BookDto> getBooks() {
        List<Book> books = bookRepository.findAll();
        List<BookDto> bookDtos = books.stream()
                .map((product) -> (BookMapper.mapToBookDto(product)))
                .collect(Collectors.toList());
        return bookDtos;
    }

    // Implementasi metode pencarian
    @Override
    public List<BookDto> searchBooks(String keyword) {
        List<Book> foundBooks = bookRepository.searchBook(keyword);

        // Mengkonversi dari entitas Book ke BookDto
        List<BookDto> bookDtos = foundBooks.stream()
                .map(book -> mapToBookDto(book))
                .collect(Collectors.toList());

        return bookDtos;
    }
}
